use std::vec::Vec;

pub fn format_bytes_as_array(data : &Vec::<u8>) -> String
{
    let mut res = String::new();
    res.push_str(&format!("unsigned char byte_array_hex[{}u] = {{ ", data.len()));
    for i in 0..data.len() 
    {
        res.push_str(&format!("{:#04X}", data[i]));
        if i != data.len() - 1
        {
            res.push_str(", ");
        }
    }
    res.push_str(" };");
    res
}

pub fn format_bytes_as_string(data : &Vec::<u8>) -> String
{
    let mut res = String::new();
    res.push_str(&format!("unsigned char string_hex[{}u] = \"", data.len() + 1)); // + 1 for null-terminating symbol
    
    for i in 0..data.len() 
    {
        res.push_str(&format!("\\x{:02X}", data[i]));
//        if i % 16 == 15 && i != data.len() - 1
//        {
//            res.push_str("\"\n                                                \"");
//        }
    }

    res.push_str("\";");
    res
}

pub fn format_int_as_array<T>(data : &Vec::<T>) -> String
    where T : std::fmt::UpperHex
{
    let mut res = String::new();
    let type_str : String;
    match std::mem::size_of::<T>()
    {
        1 => type_str = String::from("char"),
        2 => type_str = String::from("short"),
        4 => type_str = String::from("int"),
        8 => type_str = String::from("long long"),
        _ => type_str = String::from("int")
    }
    res.push_str(&format!("unsigned {0} array_hex[{1}u] = {{ ", type_str, data.len()));
    for i in 0..data.len()
    {
        res.push_str(&format!("{data:#00$X}", std::mem::size_of::<T>()*2 + 2, data=data[i])); // + 2 for 0x prefix
        if i != data.len() - 1
        {
            res.push_str(", ");
        }
    }
    res.push_str(" };");
    res
}