use std::vec::Vec;
use rand::Rng;
use rand::distributions::{Distribution, Standard};

pub fn generate_rand_array<T>(size: usize) -> Vec<T>
    where Standard: Distribution<T>
{
    rand::thread_rng().sample_iter(Standard).take(size).collect()
}
