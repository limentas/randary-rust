use iced::{ button, Application, Button, Column, Row, Container, Text, Radio, Scrollable, scrollable, TextInput, text_input, Command, Element, 
    Align, HorizontalAlignment, Background, Color, Length };

pub struct IcedUi 
{
    generated_text_line1: String,
    generated_text_line2: String,
    generate_button: button::State,
    generated_text_scroll: scrollable::State,
    selected_choice: Option<ElementType>,
    size_text_state: text_input::State,
    size_text: String
}

impl Default for IcedUi 
{
    fn default() -> IcedUi 
    {
        IcedUi 
        {
            generated_text_line1: String::from(""),
            generated_text_line2: String::from(""),
            generate_button: button::State::new(),
            generated_text_scroll: scrollable::State::new(),
            selected_choice: Some(ElementType::UByte),
            size_text_state: text_input::State::new(),
            size_text: String::from("16")
        }
    }
}

#[derive(Debug, Clone)]
pub enum Message 
{
    GeneratePressed,
    RadioSelected(ElementType),
    SizeTextChanged(String)
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum ElementType 
{
    UByte,
    UInt16,
    UInt32,
    UInt64
}

impl Application for IcedUi 
{
    type Message = Message;

    fn new() -> (Self, Command<Message>) 
    {
        (Self::default(), Command::none())
    }

    fn title(&self) -> String 
    {
        String::from("Randary")
    }

    fn update(&mut self, message: Message) -> Command<Message> 
    {
        match message 
        {
            Message::GeneratePressed =>
            {
                let mut size = self.size_text.parse::<usize>();
                if size.is_err()
                {
                    self.size_text = String::from("16");
                    size = Ok(16);
                }
                match self.selected_choice
                {
                    Some(ElementType::UByte) =>
                    {
                        let data = super::generator::generate_rand_array::<u8>(size.unwrap());
                        self.generated_text_line1 = String::new();
                        self.generated_text_line2 = String::new();
                        self.generated_text_line1.push_str(&super::formatter::format_int_as_array(&data));
                        self.generated_text_line2.push_str(&super::formatter::format_bytes_as_string(&data));
                    }
                    Some(ElementType::UInt16) =>
                    {
                        let data = super::generator::generate_rand_array::<u16>(size.unwrap());
                        self.generated_text_line1 = super::formatter::format_int_as_array(&data);
                        self.generated_text_line2 = String::new();
                    }
                    Some(ElementType::UInt32) =>
                    {
                        let data = super::generator::generate_rand_array::<u32>(size.unwrap());
                        self.generated_text_line1 = super::formatter::format_int_as_array(&data);
                        self.generated_text_line2 = String::new();
                    }
                    Some(ElementType::UInt64) =>
                    {
                        let data = super::generator::generate_rand_array::<u64>(size.unwrap());
                        self.generated_text_line1 = super::formatter::format_int_as_array(&data);
                        self.generated_text_line2 = String::new();
                    }
                    _ =>
                    {
                        self.generated_text_line1 = String::from("");
                    } 
                }
            }
            Message::RadioSelected(choise) =>
            {
                self.selected_choice = Some(choise)
            }
            Message::SizeTextChanged(text) =>
            {
                self.size_text = text
            }
        }

        Command::none()
    }

    fn view(&mut self) -> Element<Message> 
    {
        Column::new().align_items(Align::Center).spacing(50).padding(50)
            .push(Text::new("This online tool allows you to generate pseudo-random arrays of numbers and formats them in C/C++ style. It is written in Rust and Iced."))
            .push(Row::new().align_items(Align::Center).spacing(20).padding(30).max_width(700)
                .push(Column::new()
                    .push(Radio::new(ElementType::UByte, "unsigned byte", self.selected_choice, Message::RadioSelected))
                    .push(Radio::new(ElementType::UInt16, "unsigned int16", self.selected_choice, Message::RadioSelected))
                    .push(Radio::new(ElementType::UInt32, "unsigned int32", self.selected_choice, Message::RadioSelected))
                    .push(Radio::new(ElementType::UInt64, "unsigned int64", self.selected_choice, Message::RadioSelected)))
                .push(Column::new()
                    .push(Text::new("Array size:"))
                    .push(Container::new(TextInput::new(&mut self.size_text_state, "16", &self.size_text, Message::SizeTextChanged)
                            .padding(10)
                            .width(Length::Shrink))
                        .max_width(100))))
            .push(Container::new(Button::new(&mut self.generate_button, Text::new("Generate"))
                    .border_radius(10)
                    .padding(10)
                    .background(Background::Color(Color { r: 0.878, g: 0.878, b: 0.878, a: 1.0 }))
                    .on_press(Message::GeneratePressed))
                .max_width(300)
                .center_x()
                .center_y(),)
            .push(Scrollable::new(&mut self.generated_text_scroll)
                    .push(Text::new(self.generated_text_line1.clone()).size(20).horizontal_alignment(HorizontalAlignment::Left),)
                    .push(Text::new(self.generated_text_line2.clone()).size(20).horizontal_alignment(HorizontalAlignment::Left),))
            .into()
    }
}

