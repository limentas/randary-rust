pub mod generator;
pub mod iced_ui;
pub mod formatter;

use iced::{Settings, Application};

fn main()
{
    iced_ui::IcedUi::run(Settings::default())
}
